import React from 'react'

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'

import Scooters from './features/scooters'

import styles from './App.module.css'

function App() {
    return (
        <Router>
            <div className={styles.app}>
                <ul>
                    <li>
                        <Link to="/">Scooters</Link>
                    </li>
                </ul>
                <hr />
                <Switch>
                    <Route exact path="/">
                        <Scooters />
                    </Route>
                </Switch>
            </div>
        </Router>
    )
}

export default App
