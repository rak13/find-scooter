import React from 'react'
import ScootersForm from './ScootersForm'
import Map from './Map'

import styles from './Styles.module.css'

export default function Scooters() {
    return (
        <div className={styles.wrapper}>
            <ScootersForm />
            <br />
            <Map />
        </div>
    )
}
