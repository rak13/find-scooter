import { createSlice } from '@reduxjs/toolkit'
import { Loader } from '@googlemaps/js-api-loader'

import client from '../../app/client'
import { DEFAULT_LAT, DEFAULT_LNG, DEFAULT_ZOOM } from '../../app/constants'

import { G_MAP_KEY } from '../../app/config'

export const scootersSlice = createSlice({
    name: 'scooters',
    initialState: {
        scooterData: [],
        isMapLoaded: false,
        nearestScooters: [],
        toast: {
            show: false,
            message: {}
        }
    },
    reducers: {
        addData: (state, action) => {
            state.scooterData = [...state.scooterData, ...action.payload]
        },
        reloadData: (state, action) => {
            state.scooterData = action.payload
        },
        clearData: state => {
            state.scooterData = []
        },
        mapLoaded: state => {
            state.isMapLoaded = true
        },
        toast: (state, action) => {
            state.toast = { ...state.toast, ...action.payload }
        },
        nearestScooters: (state, action) => {
            state.nearestScooters = action.payload
        }
    }
})

export const loadMap = () => {
    return dispatch => {
        const loader = new Loader({
            apiKey: G_MAP_KEY,
            version: 'weekly'
        })
        loader.load().then(() => {
            window.scootersMap = new window.google.maps.Map(document.getElementById('scootersMap'), {
                center: { lat: DEFAULT_LAT, lng: DEFAULT_LNG },
                zoom: DEFAULT_ZOOM
            })

            dispatch(scootersSlice.actions.mapLoaded())
        })
    }
}

export const fetchData = () => {
    return async (dispatch, getState) => {
        const response = await client.get('/all')
        dispatch(scootersSlice.actions.reloadData(response.data))
        dispatch(
            scootersSlice.actions.toast({
                show: true,
                message: {
                    header: 'Data Fetched',
                    body: `${response.data.length} data fetched`
                }
            })
        )
    }
}

export const getNearestKScooters = (lat, lng, radius, x) => {
    return async (dispatch, getState) => {
        const response = await client.post('/nearest', { lat, lng, radius, x })
        dispatch(scootersSlice.actions.nearestScooters(response.data))
    }
}

export const uploadData = () => {
    return async (dispatch, getState) => {
        const data = getState().scooters.scooterData
        await client.post('/add', data)
        console.log('upload success')
        dispatch(
            scootersSlice.actions.toast({
                show: true,
                message: { header: 'Data uploaded ', body: `${data.length} data uploaded` }
            })
        )
        setTimeout(() => dispatch(fetchData()), 2000)
    }
}

export const clearDbData = () => {
    return async (dispatch, getState) => {
        await client.delete('/all')
        dispatch(
            scootersSlice.actions.toast({
                show: true,
                message: { header: 'All Data Deleted' }
            })
        )
        setTimeout(() => dispatch(fetchData()), 2000)
    }
}

export default scootersSlice.reducer

export const getState = state => state.counter

export const { addData, clearData, toast } = scootersSlice.actions
