import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { Button, Col, FormControl, InputGroup, Row } from 'react-bootstrap'

import DataForm from './DataForm'

import CONSTANTS from '../../app/constants'
import { distanceInMeters, map as mapUtils } from '../../app/utils'

import { getNearestKScooters } from './scootersSlice'

import styles from './Styles.module.css'

const { createCircle, createMarker, MARKER_ICONS } = mapUtils

export default function LocateScooter() {
    const dispatch = useDispatch()
    const state = useSelector(state => state.scooters)
    const { scooterData, nearestScooters } = state

    const [testLat, setTestLat] = useState(CONSTANTS.DEFAULT_LAT)
    const [testLng, setTestLng] = useState(CONSTANTS.DEFAULT_LNG)
    const [testRadius, setTestRadius] = useState(CONSTANTS.DEFAULT_RADIUS)
    const [testX, setTestX] = useState(CONSTANTS.DEFAULT_X)

    const [mapMarkers, setMapMarkers] = useState([])
    const [mapCircles, setMapCircles] = useState([])

    useEffect(() => {
        nearestScooters.forEach(scooter => {
            mapMarkers.push(createMarker(window.scootersMap, scooter.lat, scooter.lng, { icon: MARKER_ICONS.orange }))
        })
        setMapMarkers(mapMarkers)
        // eslint-disable-next-line
    }, [nearestScooters])

    function displayCenter() {
        clearFromMap(mapCircles)
        setMapCircles([])

        mapCircles.push(createCircle(window.scootersMap, testLat, testLng, 15, { fillOpacity: 1 }))
        mapCircles.push(createCircle(window.scootersMap, testLat, testLng, testRadius))
        setMapCircles(mapCircles)
    }

    function displayScooters() {
        clearFromMap(mapMarkers)
        setMapMarkers([])

        scooterData.forEach(data => {
            mapMarkers.push(createMarker(window.scootersMap, data.lat, data.lng, { icon: MARKER_ICONS.blue }))
        })
        setMapMarkers(mapMarkers)
    }

    function displayAll() {
        clearMap()
        displayCenter()
        displayScooters()
    }

    function clearFromMap(items) {
        items.forEach(o => o.setMap(null))
    }

    function clearMap() {
        clearFromMap(mapCircles)
        clearFromMap(mapMarkers)

        setMapMarkers([])
        setMapCircles([])
    }

    function getScootersInRadius(scooters) {
        return scooters
            .map(s => ({ ...s, distance: distanceInMeters(testLat, testLng, s.lat, s.lng) }))
            .filter(s => s.distance < testRadius)
    }

    function findScooters() {
        const ts1 = new Date().getTime()

        let scootersInRadius = getScootersInRadius(scooterData)

        scootersInRadius.sort((a, b) => a.distance - b.distance)

        console.log('Time taken in ms ', new Date().getTime() - ts1)
        console.log('Scooters In Radius', scootersInRadius.length)

        for (let i = 0; i < testX && i < scootersInRadius.length; i++) {
            const scooter = scootersInRadius[i]
            mapMarkers.push(createMarker(window.scootersMap, scooter.lat, scooter.lng, { label: i.toString() }))
        }
        setMapMarkers(mapMarkers)
    }

    function quickSelect(arr, k) {
        //Quick Select algorightm
        function swap(arr, i, j) {
            let temp = arr[i]
            arr[i] = arr[j]
            arr[j] = temp
        }

        function partition(arr, left, right) {
            const pivot = arr[right]

            let storeIndex = left
            for (let i = left; i < right; i++) {
                if (arr[i].distance < pivot.distance) {
                    swap(arr, storeIndex, i)
                    storeIndex++
                }
            }
            swap(arr, storeIndex, right)
            return storeIndex
        }

        let kThClosestItemIndex = 0
        let left = 0
        let right = arr.length - 1

        k = k - 1 //to match with indexing
        while (left <= right) {
            kThClosestItemIndex = partition(arr, left, right)

            if (k === kThClosestItemIndex) {
                break
            } else if (k < kThClosestItemIndex) {
                right = kThClosestItemIndex - 1
            } else {
                left = kThClosestItemIndex + 1
            }
        }

        return arr.slice(0, kThClosestItemIndex + 1)
    }

    function findScootersWithAlgo() {
        const ts1 = new Date().getTime()

        //calculate distance of each scooter from the center
        //keep only those in radius
        let scooters = getScootersInRadius(scooterData)
        const nearestXScooters = quickSelect(scooters, testX) // unordered list of nearby  X scooters

        console.log('Time taken in ms', new Date().getTime() - ts1)
        console.log('Nearest X Scooters', nearestXScooters)

        nearestXScooters.forEach(scooter => {
            mapMarkers.push(createMarker(window.scootersMap, scooter.lat, scooter.lng, { icon: MARKER_ICONS.green }))
        })
        setMapMarkers(mapMarkers)
    }

    return (
        <div>
            <DataForm />
            <hr />
            <div className={styles.header}>Try it out!</div>
            <h1 className={styles.subHeader}>Set Center, Radius and X:</h1>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Latitude</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl type="number" value={testLat} onChange={event => setTestLat(Number(event.target.value))} />
                <div className={styles.spaceLeft} />
                <InputGroup.Prepend>
                    <InputGroup.Text>Longitude</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl type="number" value={testLng} onChange={event => setTestLng(Number(event.target.value))} />

                <div className={styles.spaceLeft} />
                <InputGroup.Prepend>
                    <InputGroup.Text>Radius (in meters)</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    type="number"
                    value={testRadius}
                    onChange={event => setTestRadius(Number(event.target.value))}
                />

                <div className={styles.spaceLeft} />
                <InputGroup.Prepend>
                    <InputGroup.Text>X bikes (X)</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl type="number" value={testX} onChange={event => setTestX(Number(event.target.value))} />
            </InputGroup>

            <div className={styles.spaceTop}>
                <Button variant="outline-warning" onClick={displayScooters}>
                    Display Scooters
                </Button>
                <Button variant="outline-primary" className={styles.spaceLeft} onClick={displayCenter}>
                    Display Center
                </Button>
                <Button variant="outline-warning" className={styles.spaceLeft} onClick={displayAll}>
                    Display All
                </Button>

                <Button variant="info" className={styles.spaceLeft} onClick={clearMap}>
                    Clear Map
                </Button>
            </div>
            <hr className={styles.spaceTop} />
            <h1 className={styles.subHeader}>Find X Nearest Scooters in Radius</h1>

            <div className={styles.spaceTop}>
                <Row>
                    <Col>
                        <div className={styles.infoLabel}>Scooter data available: {scooterData.length}</div>
                    </Col>
                    <Col>
                        <div className={styles.infoLabel}>
                            Scooters in radius: {getScootersInRadius(scooterData).length}
                        </div>
                    </Col>
                </Row>
                <div style={{ fontSize: '15px', color: 'red' }}>
                    *Please upload your data for consistent results with backend
                </div>
            </div>
            <div className={styles.spaceTop}>
                <Button variant="secondary" onClick={findScooters}>
                    <div>Sort Scooters by distance</div>
                    <div>and Mark X</div>
                </Button>

                <Button className={styles.spaceLeft} variant="success" onClick={findScootersWithAlgo}>
                    <div>Find X scooter in O(n) </div>
                    <div>(using front-end data)</div>
                </Button>

                <Button
                    className={styles.spaceLeft}
                    variant="warning"
                    onClick={() => dispatch(getNearestKScooters(testLat, testLng, testRadius, testX))}
                >
                    <div>Find X scooter in O(n) </div>
                    <div>(using back-end data)</div>
                </Button>
            </div>
        </div>
    )
}
