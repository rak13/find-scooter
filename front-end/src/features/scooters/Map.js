import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'

import { loadMap } from './scootersSlice'

import styles from './Styles.module.css'

export default function Map() {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(loadMap())
    })

    return <div id="scootersMap" className={styles.map} />
}
