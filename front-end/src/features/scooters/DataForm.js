import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Button, FormControl, InputGroup, Toast } from 'react-bootstrap'

import CONSTANTS from '../../app/constants'
import { getRandomNumber } from '../../app/utils'

import { addData, clearData, clearDbData, toast, uploadData, fetchData } from './scootersSlice'

import styles from './Styles.module.css'

export default function DataForm() {
    const dispatch = useDispatch()
    const scootersState = useSelector(state => state.scooters)

    useEffect(() => {
        dispatch(fetchData())
    }, [dispatch])

    const scooterData = scootersState.scooterData

    const [dataSize, setDataSize] = useState(20)
    const [customLat, setCustomLat] = useState(CONSTANTS.DEFAULT_LAT)
    const [customLng, setCustomLng] = useState(CONSTANTS.DEFAULT_LNG)

    function generateData() {
        let n = dataSize
        const data = []
        while (n--) {
            const lat = getRandomNumber(CONSTANTS.MIN_LAT, CONSTANTS.MAX_LAT)
            const lng = getRandomNumber(CONSTANTS.MIN_LNG, CONSTANTS.MAX_LNG)
            data.push({ id: scooterData.length + data.length, lat, lng })
        }

        dispatch(addData(data))
    }

    function addCustomData() {
        dispatch(
            addData([
                {
                    id: scooterData.length,
                    lat: customLat,
                    lng: customLng
                }
            ])
        )
    }

    function logData() {
        console.log({ scooterData })
    }

    return (
        <div>
            <Toast
                onClose={() =>
                    dispatch(
                        toast({
                            show: false,
                            message: {}
                        })
                    )
                }
                show={scootersState.toast.show}
                delay={5000}
                autohide
                animation
                className={styles.toast}
            >
                <Toast.Header closeButton={false}>
                    <strong className="mr-auto">{scootersState.toast.message.header}</strong>
                </Toast.Header>
                <Toast.Body>{scootersState.toast.message.body}</Toast.Body>
            </Toast>
            <div className={styles.header}>Data!</div>
            <div className={styles.subHeader}>Generate Random Data:</div>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Number of data to generate</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    type="number"
                    value={dataSize}
                    onChange={event => setDataSize(Number(event.target.value))}
                />
                <Button variant="outline-primary" onClick={generateData} className={styles.spaceLeft}>
                    Generate
                </Button>
            </InputGroup>
            <div className={styles.infoLabel}>Number of data available: {scooterData.length}</div>
            <h1 className={styles.subHeader}>Add custom data:</h1>
            <InputGroup className="mb-3">
                <InputGroup.Prepend>
                    <InputGroup.Text>Latitude</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    type="number"
                    value={customLat}
                    onChange={event => setCustomLat(Number(event.target.value))}
                />
                <div className={styles.spaceLeft} />
                <InputGroup.Prepend>
                    <InputGroup.Text>Longitude</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    type="number"
                    value={customLng}
                    onChange={event => setCustomLng(Number(event.target.value))}
                />

                <Button variant="outline-primary" onClick={addCustomData} className={styles.spaceLeft}>
                    Add
                </Button>
            </InputGroup>
            <div style={{ fontSize: '15px', color: 'blue' }}>
                "Genrate" and "Add" buttons only add scooter locations to front-end data - use "Upload Data" button
                below to save to database
            </div>
            <div style={{ fontSize: '15px', color: 'red' }}>
                *input validations not yet implemented - please use valid floating numbers
            </div>
            <div className={styles.spaceY}>
                <Button variant="light" onClick={logData}>
                    Log Data
                </Button>
                <Button variant="light" className={styles.spaceLeft} onClick={() => dispatch(clearData())}>
                    Clear Local Data
                </Button>
                <Button variant="outline-success" className={styles.spaceLeft} onClick={() => dispatch(uploadData())}>
                    Upload Data
                </Button>
                <Button variant="outline-danger" className={styles.spaceLeft} onClick={() => dispatch(clearDbData())}>
                    Clear Database
                </Button>
            </div>
        </div>
    )
}
