const CONSTANTS = {
    DEFAULT_LAT: 1.347,
    DEFAULT_LNG: 103.741,
    DEFAULT_RADIUS: 1500,
    DEFAULT_X: 7,

    DEFAULT_ZOOM: 15,

    MIN_LAT: 1.32259, //bottom
    MAX_LAT: 1.38523, //top

    MIN_LNG: 103.70975, //left
    MAX_LNG: 103.78898 //right
}

module.exports = CONSTANTS
