import { configureStore } from '@reduxjs/toolkit'

import scootersReducer from '../features/scooters/scootersSlice'

export default configureStore({
    reducer: {
        scooters: scootersReducer
    }
})
