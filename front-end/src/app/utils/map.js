function createMarker(map, lat, lng, options) {
    return new window.google.maps.Marker({
        position: { lat, lng },
        map,
        ...options
    })
}

function createCircle(map, lat, lng, radius, options) {
    return new window.google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.05,
        map,
        center: { lat, lng },
        radius,
        ...options
    })
}

const iconBase = 'http://maps.google.com/mapfiles/ms/icons/'
const MARKER_ICONS = {
    blue: `${iconBase}blue-dot.png`,
    green: `${iconBase}green-dot.png`,
    yellow: `${iconBase}orange-dot.png`,
    orange: `${iconBase}orange-dot.png`,
    red: `${iconBase}red-dot.png`
}

export { createMarker, createCircle, MARKER_ICONS }
