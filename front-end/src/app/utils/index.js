import * as map from './map'

function getRandomNumber(min, max, decimalPlace = 5) {
    const num = Math.random() * (max - min) + min
    return Number(num.toFixed(decimalPlace))
}

function degreesToRadians(degrees) {
    return (degrees * Math.PI) / 180
}

function distanceInMeters(lat1, lng1, lat2, lng2) {
    var earthRadiusMeters = 6371 * 1000

    var dLat = degreesToRadians(lat2 - lat1)
    var dlng = degreesToRadians(lng2 - lng1)

    lat1 = degreesToRadians(lat1)
    lat2 = degreesToRadians(lat2)

    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dlng / 2) * Math.sin(dlng / 2) * Math.cos(lat1) * Math.cos(lat2)
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    return earthRadiusMeters * c
}

export { getRandomNumber, distanceInMeters, map }
