import React from 'react'
import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux'
import store from './app/store'
import App from './App'

test('renders Data header', () => {
    const { getByText } = render(
        <Provider store={store}>
            <App />
        </Provider>
    )

    expect(getByText('Data')).toBeInTheDocument()
})

test('renders Try it out!', () => {
    const { getByText } = render(
        <Provider store={store}>
            <App />
        </Provider>
    )

    expect(getByText(/try it out/i)).toBeInTheDocument()
})
