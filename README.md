# Beam Scooter Search

This project contains 2 parts. The back-end and the front-end.

## Installation

Requirements:

- [Node.js](https://nodejs.org/en/) environment
- [yarn](https://classic.yarnpkg.com/en/docs/install) package manager

The project was tested on node v12.20.0, and yarn v1.22.10

```
git clone https://bitbucket.org/rak13/find-scooter.git
```

```bash
cd back-end
yarn install
yarn start
```

Open a new terminal

```bash
cd front-end
yarn install
yarn start
```

Back-end test

```bash
cd back-end
yarn test
```

## Front-end

It was built using react-redux frameworks

There are two sections

- Data
- Try it out!

### Data

It allows you modify the data to your needs

![data](./readme-images/data.png?raw=true)

- "Generate" a given number of scooter data and add to local data. Random data is generated within these lattitude, longitude bounds

```
   MIN_LAT: 1.32259, //bottom
   MAX_LAT: 1.38523, //top

   MIN_LNG: 103.70975, //left
   MAX_LNG: 103.78898 //right
```

- "Add" custom scooter data - add to front-end data
- "Log Data" - check browsers console to see current available data
- "Clear local data" - clears all front-end data. Data in database remains unchanged. Data from database can be reloaded by refresh.
- "Upload data" - uploads current data to backed. Backend data can be replaced.
- "Clear database" - clears all data from the back-end database

Data is generated in the below format. The "id" is grenerated from the length of the currently available data.
Therefore if data is uploaded the data info in database can be replaced if the same id exists.

```
id: 19
lat: 1.35095
lng: 103.75484
```

### Try it out

This section lets you set the center and radius and X number bikes to look

![map](./readme-images/map.png?raw=true)

- Display scooters - show all scooters with blue markers
- Display Center - show center and circle in red color
- Display All - show both circle and center
- Clear Map - remove all markers and circles on map

There three buttons that suggest the approach taken to find and mark locations of nearby scooters.

The "Sort Scooters by distance buttons" takes the naive approach (of sorting scooters by distance from the center) and also labels the scooters based on their distance. The scooter closest to center will be labeled as 0.

![maplabel](./readme-images/map-label.png?raw=true)

The other two approches use QuickSelect algorithms to get an unordered list on X closest scooters. The green button applies this approach in to the front-end data and the yellow does the same with in the backend from the data in DB.

Markers on the map may overlap while displaying and may not be visible at times. Please clear the map as often as required.

### Sample usage

#### #Using existing data from data base

```
 - "Try it out section"
 - Clear Map
 - Display all => Clear Map => Display Center => Sort Scooters by distance
 - Clear Map => Display Center => Find X scooter in O(n) (using front-end..)
 - Clear Map => Display Center => Find X scooter in O(n) (using back-end...)
 - Clear Map => Set X to 20 => Display Center => Find X scooter in O(n) (using back-end...)
 - Clear Map => Set Radius to 200 => Set X bikes to 2
 - Display All => Zoom in on the map towards the circle => Sort Scooters by distance
 - Clear Map => Display Center => Find X scooter in O(n) (using front-end..)
 - Clear Map => Find X scooter in O(n) (using back-end...)
 - Set X to 10 => Clear Map each time and try those 3 buttons
 - Set Latitude: 1.346, Longitude: 103.740 => Try out those buttons!
 - Remember to clear the map
```

#### #Use custom data

```
- Clear Map => Clear Local Data
- Check if "Number of data available: 0"
- Set these values for Latitude and Longitude in "Data!" section and click "Add". You may also click on  "Display All" after each "Add"
    => Latitude: 1.347, Longitude: 103.740
    => Latitude: 1.347, Longitude: 103.742
    => Latitude: 1.343, Longitude: 103.741
    => Latitude: 1.343, Longitude: 103.745
    => Latitude: 1.339, Longitude: 103.741
    => Latitude: 1.349, Longitude: 103.743
    => Latitude: 1.350, Longitude: 103.740
    => Latitude: 1.350, Longitude: 103.740
    => Latitude: 1.352, Longitude: 103.739
- Set Radius to 500 in the "Try it out" section
- Set X bikes to 3
- Clear Map => Display All => Sort Scooters by distance => Find X scooter in O(n) (using front-end...)
- Clear Map => Display All => Find X scooter in O(n) (using front-end...)

* Data is not in sync with server database, so "Find X scooter in O(n) (using back-end...)" will not show expected results.
To sync with DB,  "Clear Database" =>  "Uplaod Data" => Repeat above process of adding custom data
```

#### #Use random data

```
- Check value of `Number of data available`
- Set "Number of data to generate" to 1000000 -  give it a few seconds - check value of "Number of data available" increases
- Clear Map => Display Center => Sort Scooters by distance
- Clear Map => Display Center => Find X scooter in O(n) (using front-end..)

```

Open the browser's console. Note the values for "Time taken in ms".
Try again. Click on those "Sort..." and "Find..." buttons. Which one has better runtime?

## Back-end

It was built on Node.Js+ExpressJs framework

SQLite3 was used as database

## Scaleability (System design)
[Scale for opertaions team](https://docs.google.com/document/d/1kkTMy-1qPbf-GmC5ugvgbX2raMim32dzq7n7X7_sOIk/edit?usp=sharing)

## Contact

[Rakib Ahsan](https://www.linkedin.com/in/rak13/)
