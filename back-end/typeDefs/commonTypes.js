/**
 * @typedef {{lat: {min: number, max: number}, lng: {min: number, max: number}}} Bounds
 *
 * @typedef {{id: number, lat: number, lng: number}} Scooter
 */
