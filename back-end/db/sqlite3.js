const sqlite3 = require('sqlite3').verbose()
const { DB_FILE_PATH } = require('../config')

const db = new sqlite3.Database(DB_FILE_PATH, err => {
    if (err) {
        console.error(err.message)
        return
    }
    console.log('Connected to the sqlite3 database.')
})

module.exports = db
