process.env.BEAM_STAGE = 'test'

const chance = new (require('chance'))()
const chai = require('chai')
chai.use(require('chai-http'))
const expect = chai.expect

const server = require('../server')
const db = require('../db/sqlite3')
const { distanceInMeters } = require('../routes/api/utils')
const ScooterDao = require('../dao/scooter')

describe('Test Scooters', () => {
    function generateScooters(N, startingId = 0, min, max) {
        const scooters = []
        while (N--) {
            scooters.push({
                id: startingId++,
                lat: Number(chance.floating({ min: min || 1.38523, max: max || 1.38523 }).toFixed(5)),
                lng: Number(chance.floating({ min: min || 103.70975, max: max || 103.78898 }).toFixed(5))
            })
        }
        return scooters
    }

    let scooters = []
    let requester = null
    before(async () => {
        requester = chai.request(server).keepOpen()
    })

    beforeEach(async () => {
        await ScooterDao.deleteAllScooters()
        scooters = generateScooters(chance.integer({ min: 100, max: 200 }))
        await ScooterDao.insertScooters(scooters)
    })

    after(async () => {
        await ScooterDao.deleteAllScooters()
    })

    it('it should get all scooters', async () => {
        const response = await requester.get('/all')
        expect(response).to.have.status(200)
        expect(response.body).to.be.a('array')
        expect(response.body.length).to.be.equal(scooters.length)
        expect(response.body).to.be.deep.equal(scooters)
    })

    it('it should fail while adding an invalid scooter', async () => {
        const newScooter = {
            id: 'A'
        }

        const postResponse = await requester.post('/add').send(newScooter)

        expect(postResponse).to.have.status(400)
        expect(postResponse.body).to.be.deep.equal({ code: 'SQLITE_MISMATCH', message: 'Error adding scooter(s)' })
    })

    it('it should add a new scooter', async () => {
        const newScooter = generateScooters(1, scooters.length)[0]

        const postResponse = await requester.post('/add').send(newScooter)
        expect(postResponse).to.have.status(200)
        expect(postResponse.body).to.be.deep.equal({ message: '1 items added' })

        const getResponse = await requester.get('/all')

        expect(getResponse).to.have.status(200)
        expect(getResponse.body).to.be.a('array')
        expect(getResponse.body.length).to.be.equal(scooters.length + 1)
        expect(getResponse.body).to.be.deep.equal([...scooters, newScooter])
    })

    it('it should add a random number of new scooters', async () => {
        const newScooters = generateScooters(chance.integer({ min: 100, max: 1000 }), scooters.length)

        const postResponse = await requester.post('/add').send(newScooters)
        expect(postResponse).to.have.status(200)
        expect(postResponse.body).to.be.deep.equal({ message: `${newScooters.length} items added` })

        const getResponse = await requester.get('/all')

        expect(getResponse).to.have.status(200)
        expect(getResponse.body).to.be.a('array')
        expect(getResponse.body.length).to.be.equal(scooters.length + newScooters.length)
        expect(getResponse.body).to.be.deep.equal([...scooters, ...newScooters])
    })

    it('it should delete all scooters', async () => {
        const response = await requester.delete('/all').send()
        expect(response).to.have.status(200)
        expect(response.body).to.be.deep.equal({ message: 'All items deleted' })

        const getResponse = await requester.get('/all')

        expect(getResponse).to.have.status(200)
        expect(getResponse.body).to.be.a('array')
        expect(getResponse.body.length).to.be.equal(0)
        expect(getResponse.body).to.be.deep.equal([])
    })

    it('it should return all scooters within bounds', async () => {
        // only the new scooters are in bounds
        const newScooters = generateScooters(chance.integer({ min: 100, max: 200 }), scooters.length, 250, 350)
        await requester.post('/add').send(newScooters)
        const bounds = {
            lat: {
                min: 200,
                max: 400
            },
            lng: {
                min: 200,
                max: 400
            }
        }

        scootersWithinBounds = await ScooterDao.getScootersWithinBounds(bounds)
        expect(scootersWithinBounds).to.be.a('array')
        expect(scootersWithinBounds.length).to.be.equal(newScooters.length)
        expect(scootersWithinBounds).to.be.deep.equal(newScooters)
    })

    for (let testRun = 0; testRun < 50; testRun++) {
        it(`it should return X scooters within radius - testRun ${testRun}`, async () => {
            for (let j = 0; j < 50; j++) {
                //choose random scooter location as center
                const center = scooters[chance.integer({ min: 0, max: scooters.length - 1 })]

                //add distance property to scooters and sort by distance
                //i.e scooters closest to center appear first in the array
                const _scooters = scooters
                    .map(s => ({ ...s, distance: distanceInMeters(center.lat, center.lng, s.lat, s.lng) }))
                    .sort((s1, s2) => s1.distance - s2.distance)

                const indexOfCenter = _scooters.findIndex(s => s.id === center.id)

                //choose another scooter as radius and consider distance of that from center as radius
                const edge = _scooters[chance.integer({ min: indexOfCenter + 1, max: _scooters.length - 1 })]

                const radius = distanceInMeters(center.lat, center.lng, edge.lat, edge.lng)

                const randomX = chance.integer({ min: 0, max: scooters.length * 2 })

                //scan and find X nearest scooters to center
                //expected results
                const scannedXScooters = _scooters.filter(s => s.distance < radius).filter((s, i) => i < randomX)

                const response = await requester
                    .post('/nearest')
                    .send({ lat: center.lat, lng: center.lng, radius, x: randomX })

                expect(response).to.have.status(200)
                expect(response.body).to.be.a('array')
                expect(response.body.length).to.be.equal(scannedXScooters.length)

                expect(response.body.map(s => s.distance).sort((d1, d2) => d1 - d2)).to.be.deep.equal(
                    scannedXScooters.map(s => s.distance)
                )
            }
        })
    }
})
