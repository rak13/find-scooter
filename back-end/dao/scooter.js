const typeDefs = require('../typeDefs/commonTypes')

//Data Access Object (DAO) for scooters

const db = require('../db/sqlite3')
const { promisify } = require('util')

const TableName = 'Scooters'

function createTable() {
    db.serialize(() => {
        const query = `
            CREATE TABLE IF NOT EXISTS Scooters (
                id INTEGER PRIMARY KEY,
                lat NUMBER NOT NULL,
                lng NUMBER NOT NULL
            );
        `
        db.run(query)
    })
}

/**
 * Promisify db.run function
 *
 * @param {string} query query string
 * @param {Array} params params
 */
const dbRunPromise = (query, params) => {
    return new Promise((resolve, reject) => {
        db.run(query, params, function (err) {
            if (err) {
                return reject(err)
            } else {
                resolve(this.lastID)
            }
        })
    })
}

/**
 * Inserts array of scooters into database
 *
 * @param {Scooter[]} scooters Array of scooters
 */
async function insertScooters(scooters) {
    //TODO: perform parallel/bulk inserts
    for (let scooter of scooters) {
        await dbRunPromise(`INSERT OR REPLACE INTO ${TableName}(id, lat, lng) values (?, ?, ?)`, [
            scooter.id,
            scooter.lat,
            scooter.lng
        ])
    }
}

/**
 * Inserts array of scooters into database
 * @returns {Scooter[]} Array of scooters
 */
async function getAllScooters() {
    const all = promisify(db.all).bind(db)
    return (await all(`SELECT * FROM ${TableName}`)) || []
}

/**
 * Deletes all rows from the DB
 *
 */
async function deleteAllScooters() {
    return await dbRunPromise(`DELETE FROM ${TableName}`)
}

/**
 * Returns array of scooters within the given bounds
 *
 * @param {Bounds} bounds
 * @return {Scooter[]} array of scooters
 */
async function getScootersWithinBounds(bounds) {
    const query = `
        SELECT * FROM  ${TableName} 
        WHERE lat BETWEEN ${bounds.lat.min} AND ${bounds.lat.max}
        AND lng BETWEEN ${bounds.lng.min} AND ${bounds.lng.max}
    `
    const all = promisify(db.all).bind(db)
    return (await all(query)) || []
}

module.exports = {
    createTable,
    deleteAllScooters,
    getAllScooters,
    getScootersWithinBounds,
    insertScooters,
    TableName
}
