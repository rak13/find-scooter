const typeDefs = require('../../typeDefs/commonTypes')

/**
 * Converts degrees to radians
 *
 * @param {number} degrees The latitude1
 * @returns {number} radians
 */
function degreesToRadians(degrees) {
    return (degrees * Math.PI) / 180
}

/**
 * Returns earth distance in meters between two lat lng points
 *
 * @param {number} lat1 The latitude1
 * @param {number} lng1 The longitude1
 * @param {number} lat2 The latitude1
 * @param {number} lng2 The longitude1
 * @returns {number} distance in meters
 */
function distanceInMeters(lat1, lng1, lat2, lng2) {
    var earthRadiusMeters = 6371 * 1000

    var dLat = degreesToRadians(lat2 - lat1)
    var dlng = degreesToRadians(lng2 - lng1)

    lat1 = degreesToRadians(lat1)
    lat2 = degreesToRadians(lat2)

    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dlng / 2) * Math.sin(dlng / 2) * Math.cos(lat1) * Math.cos(lat2)
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    return earthRadiusMeters * c
}

/**
 * Returns max and min values of lat, lng by adding distance to given lat and lng
 * The return values can be used to mark square bounds around the given lat lng
 *
 * @param {number} lat The latituee
 * @param {number} lng The longitude
 * @param {number} distance The distance in Km
 * @return {Bounds} bounds
 */
function getSquareBounds(lat, lng, distance) {
    const earth_radius = 6378.1 //km
    const eps = 0.001
    const dLat = (distance / earth_radius) * (180 / Math.PI) + eps
    const dLng = ((distance / earth_radius) * (180 / Math.PI)) / Math.cos((lat * Math.PI) / 180) + eps

    return {
        lat: {
            min: lat - dLat,
            max: lat + dLat
        },
        lng: {
            min: lng - dLng,
            max: lng + dLng
        }
    }
}

/**
 * Returns array of k nearest scooters
 * It similar to returning the first k elements from a sorted list
 * with an average case complexity of O(n)
 *
 * @param {Scooter[]} scooters The latituee
 * @param {number} k the number of closest scooters
 * @returns {Scooter[]} k nearest scooters
 */
function kNearestScooters(scooters, k) {
    if (k === 0) return []

    function swap(arr, i, j) {
        let temp = arr[i]
        arr[i] = arr[j]
        arr[j] = temp
    }

    function partition(arr, left, right) {
        const pivot = arr[right]

        let storeIndex = left
        for (let i = left; i < right; i++) {
            if (arr[i].distance < pivot.distance) {
                swap(arr, storeIndex, i)
                storeIndex++
            }
        }
        swap(arr, storeIndex, right)
        return storeIndex
    }

    let kThClosestItemIndex = 0
    let left = 0
    let right = scooters.length - 1

    k = k - 1 //to match with indexing
    while (left <= right) {
        kThClosestItemIndex = partition(scooters, left, right)

        if (k === kThClosestItemIndex) {
            break
        } else if (k < kThClosestItemIndex) {
            right = kThClosestItemIndex - 1
        } else {
            left = kThClosestItemIndex + 1
        }
    }

    return scooters.slice(0, kThClosestItemIndex + 1)
}

module.exports = {
    distanceInMeters,
    getSquareBounds,
    kNearestScooters
}
