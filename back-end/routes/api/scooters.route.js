const express = require('express')
const _ = require('lodash')

const router = express.Router()

const { deleteAllScooters, insertScooters, getAllScooters, getScootersWithinBounds } = require('../../dao/scooter')
const { distanceInMeters, getSquareBounds, kNearestScooters } = require('./utils')

router.route('/').get((req, res) => {
    res.send('Hello Beam!')
})

router.route('/nearest').post(async (req, res) => {
    const { lat, lng, radius, x } = req.body
    const bounds = getSquareBounds(lat, lng, radius / 1000)
    const results = await getScootersWithinBounds(bounds) //get scooters withing square bounds
    const scooters = results
        .map(s => ({ ...s, distance: distanceInMeters(lat, lng, s.lat, s.lng) }))
        .filter(s => s.distance < radius)

    return res.status(200).json(kNearestScooters(scooters, x))
})

router
    .route('/all')
    .get(async (req, res) => {
        try {
            const results = await getAllScooters()
            res.status(200).json(results)
        } catch (err) {
            res.status(400).json({ message: 'Error fetching all scooters' })
        }
    })
    .delete(async (req, res) => {
        try {
            const result = await deleteAllScooters()
            res.status(200).json({ message: `All items deleted` })
        } catch (err) {
            console.log('Error deleting all scooters', err)
            res.status(400).json({ message: err.message })
        }
    })

router.route('/add').post(async (req, res, next) => {
    let data = req.body
    if (!_.isArray(data)) {
        data = [data]
    }
    try {
        await insertScooters(data)
        res.status(200).json({ message: `${data.length} items added` })
    } catch (err) {
        res.status(400).json({ code: err.code, message: 'Error adding scooter(s)' })
    }
})

module.exports = router
