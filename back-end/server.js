const express = require('express')
const cors = require('cors')

const { STAGE } = require('./config')
const db = require('./db/sqlite3')
const scootersRoute = require('./routes/api/scooters.route')
const logger = require('morgan')

const scooterDao = require('./dao/scooter')
const config = require('./config')
scooterDao.createTable()

const PORT = process.env.PORT || 4000

const app = express()
app.use(
    logger('Method: :method, URL: :url, Status: :status, Response Time: :response-time', {
        skip: (req, res) => STAGE === 'test'
    })
)
app.use(cors({ origin: true }))
app.use(express.json({ limit: '50mb' }))

app.use('/', scootersRoute)

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})

module.exports = app
