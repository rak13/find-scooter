const DB_FILE_PATH = 'dev-sqlite3.db'

module.exports = {
    STAGE: 'dev',
    DB_FILE_PATH
}
