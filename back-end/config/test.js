const DB_FILE_PATH = 'test-sqlite3.db'

module.exports = {
    STAGE: 'test',
    DB_FILE_PATH
}
