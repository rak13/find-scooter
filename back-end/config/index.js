const stages = Object.freeze({
    PROD: 'prod',
    DEV: 'dev',
    TEST: 'test'
})

const config = (() => {
    switch (process.env.BEAM_STAGE) {
        case stages.PROD:
            return require(`./${stages.PROD}`)
        case stages.TEST:
            return require(`./${stages.TEST}`)
        default:
            return require(`./${stages.DEV}`)
    }
})()

module.exports = config
