const DB_FILE_PATH = 'prod-sqlite3.db'

module.exports = {
    STAGE: 'prod',
    DB_FILE_PATH
}
